<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Route::middleware('ms-gate')->group(function () {
    Route::apiResource('posts', 'PostController');
    Route::post('posts/{post}/toggle-like', 'PostController@toggleLike')->name('posts.toggle-like');

    Route::apiResource('categories', 'CategoryController');
    Route::get('categories/{category}/posts', 'PostController@getPostsByCategory')->name('categories.posts');
    Route::apiResource('tags', 'TagController')->only(['index']);
    Route::get('tags/{tag}/posts', 'PostController@getPostsByTag')->name('tags.posts');
//});
