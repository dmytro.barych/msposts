<?php

declare(strict_types=1);

namespace Tests\Feature;

use App\Http\Middleware\MsGateMiddleware;
use App\Models\Post;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class PostControllerTest extends TestCase
{
    use WithFaker;

    public function setUp(): void
    {
        parent::setUp();
        $this->withoutMiddleware([MsGateMiddleware::class]);
    }

    /**
     * @test
     */
    public function storePostTest()
    {
        $postArray = [
            'title' => Str::random(rand(3, 10)),
            'description' => $this->faker->text,
            'user_id' => 1,
            'type' => 'text'
        ];
        $response = $this->post(route('posts.store', $postArray));
        $response->assertStatus(Response::HTTP_CREATED);
        $post = $response->getOriginalContent();
        $this->assertDatabaseHas(Post::class, [
            'id' => $post->id,
            'title' => $postArray['title']
        ]);
    }

    /**
     * @test
     */
    public function updatePostTest()
    {
        $postOld = Post::factory()->create();
        $postArray = ['title' => Str::random(rand(3, 10)), 'description' => $this->faker->text, $postOld->id];
        $response = $this->put(route('posts.update', $postArray));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas(Post::class, [
            'id' => $postOld->id,
            'title' => $postArray['title'],
            'description' => $postArray['description']
        ]);
    }

    /**
     * @test
     */
    public function indexPostTest()
    {
        Post::factory()->count(5)->create();
        $response = $this->get(route('posts.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'title',
                    'description',
                    'user_id',
                    'likes_count',
                    'created_at'
                ]
            ]
        ]);
    }

    /**
     * @test
     */
    public function showPostTest()
    {
        $post = Post::factory()->create();
        $response = $this->get(route('posts.show', [$post->id]));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'data' => [
                'id',
                'title',
                'description',
                'user_id',
                'likes_count',
                'created_at'
            ]
        ]);
    }

    /**
     * @test
     */
    public function deletePostTest()
    {
        $postOld = Post::factory()->create();
        $response = $this->delete(route('posts.destroy', [$postOld->id]));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseMissing(Post::class, [
            'id' => $postOld->id
        ]);
    }

    /**
     * @test
     */
    public function togglePostLikeTest()
    {
        $post = Post::factory()->create();

        //Like
        $response = $this->post(route('posts.toggle-like', [$post->id, 'user_id' => $post->user_id]));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas('post_likes', ['post_id' => $post->id, 'user_id' => $post->user_id]);

        //Remove like
        $response = $this->post(route('posts.toggle-like', [$post->id, 'user_id' => $post->user_id]));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseMissing('post_likes', ['post_id' => $post->id, 'user_id' => $post->user_id]);
    }
}
