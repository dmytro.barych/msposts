<?php

declare(strict_types=1);

namespace App\Services;

use App\Exceptions\BasicException;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

abstract class BaseService{

    public function handleException(Exception $exception): bool
    {
        DB::rollBack();

        Log::error($exception->getMessage());
        Log::error($exception->getTraceAsString());

        throw new BasicException($exception->getMessage(), $exception->getCode(), $exception);
    }
}
