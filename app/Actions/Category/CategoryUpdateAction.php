<?php

namespace App\Actions\Category;

use App\Exceptions\BasicException;
use App\Models\Category;
use Illuminate\Support\Facades\DB;

class CategoryUpdateAction
{
    public function handle(Category $category, array $data): ?Category
    {
        try {
            DB::beginTransaction();

            $category->fill($data);
            $category->save();

            DB::commit();

            return $category;
        } catch (BasicException $exception) {
            DB::rollBack();
        }
    }
}
