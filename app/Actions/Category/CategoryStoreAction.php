<?php

declare(strict_types=1);

namespace App\Actions\Category;

use App\Exceptions\BasicException;
use App\Models\Category;
use Illuminate\Support\Facades\DB;

class CategoryStoreAction
{
    public function handle(array $data): ?Category
    {
        try {
            DB::beginTransaction();

            $category = new Category($data);
            $category->parent()->associate($data['parent_id'] ?? null);
            $category->save();

            DB::commit();

            return $category;
        } catch (BasicException $exception) {
            DB::rollBack();
        }
    }
}
