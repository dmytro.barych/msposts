<?php

declare(strict_types=1);

namespace App\Actions\Category;

use App\Exceptions\BasicException;
use App\Models\Category;
use Illuminate\Support\Facades\DB;

class CategoryDeleteAction
{
    public function handle(Category $category): bool
    {
        try {
            DB::beginTransaction();

            $category->delete();

            DB::commit();

            return true;
        } catch (BasicException $exception) {
            DB::rollBack();

            return false;
        }
    }
}
