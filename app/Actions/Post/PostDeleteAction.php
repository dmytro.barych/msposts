<?php

declare(strict_types=1);

namespace App\Actions\Post;

use App\Exceptions\BasicException;
use App\Models\Post;
use Illuminate\Support\Facades\DB;

class PostDeleteAction
{
    public function handle(Post $post): bool
    {
        try {
            DB::beginTransaction();

            $post->delete();

            DB::commit();

            return true;
        } catch (BasicException $exception) {
            DB::rollBack();

            return false;
        }
    }
}
