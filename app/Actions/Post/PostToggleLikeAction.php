<?php

declare(strict_types=1);

namespace App\Actions\Post;

use App\Exceptions\BasicException;
use App\Models\Post;
use Illuminate\Support\Facades\DB;

class PostToggleLikeAction
{
    public function handle(Post $post, int $userId): ?Post
    {
        try {
            DB::beginTransaction();

            $like = $post->likes()->where('user_id', $userId)->first();
            if ($like) {
                $like->delete();
            } else {
                $post->likes()->create(['user_id' => $userId]);
            }

            DB::commit();

            return $post;
        } catch (BasicException $exception) {
            DB::rollBack();
        }
    }
}
