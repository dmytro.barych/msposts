<?php

declare(strict_types=1);

namespace App\Actions\Post;

use App\Actions\Tag\TagsStoreAction;
use App\Exceptions\BasicException;
use App\Models\Post;
use Illuminate\Support\Facades\DB;

class PostStoreAction
{
    public function handle(array $data): ?Post
    {
        try {
            DB::beginTransaction();

            $post = new Post($data);
            $post->save();

            $post->categories()->sync($data['category_ids'] ?? null);
            $action = app()->make(TagsStoreAction::class);
            $tags = $action->handle($data['tags'] ?? []);
            $post->tags()->sync($tags->pluck('id')->toArray());

            DB::commit();

            return $post;
        } catch (BasicException $exception) {
            DB::rollBack();
        }
    }
}
