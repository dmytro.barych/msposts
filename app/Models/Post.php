<?php

declare(strict_types=1);

namespace App\Models;

use App\Traits\PostRelationsTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use Spatie\Translatable\HasTranslations;

class Post extends Model
{
    use HasFactory;
    use PostRelationsTrait;
    use Searchable;
    use HasTranslations;

    protected $fillable = ['title', 'description', 'user_id', 'type'];

    protected $dates = ['created_at'];

    public array $translatable = ['title', 'description'];

    const TYPES = [self::TYPE_TEXT, self::TYPE_PICTURE, self::TYPE_OTHER];
    const TYPE_TEXT = 'text';
    const TYPE_PICTURE = 'picture';
    const TYPE_OTHER = 'other';
}
