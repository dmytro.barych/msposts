<?php

declare(strict_types=1);

namespace App\Models;

use App\Traits\CategoryRelationsTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Kalnoy\Nestedset\NodeTrait;
use Spatie\Translatable\HasTranslations;

class Category extends Model
{
    use HasFactory;
    use NodeTrait;
    use CategoryRelationsTrait;
    use HasTranslations;

    protected $fillable = ['name', 'slug'];

    public array $translatable = ['name'];

    public function setNameAttribute($value)
    {
        $this->attributes['slug'] = Str::slug($value);
        $this->attributes['name'] = $value;
    }
}
