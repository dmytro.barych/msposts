<?php

declare(strict_types=1);

namespace App\Models;

use App\Traits\TagRelationsTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Tag extends Model
{
    use HasFactory;
    use TagRelationsTrait;
    use Searchable;

    protected $fillable = ['name'];
}
