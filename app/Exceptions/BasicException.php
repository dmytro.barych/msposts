<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;

class BasicException extends Exception
{
    public function report()
    {

    }
}
