<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\Tag\Index;
use App\Http\Resources\PostCollection;
use App\Http\Resources\TagCollection;
use App\Http\Resources\TagResource;
use App\Models\Tag;

class TagController extends Controller
{
    public function index(Index $request): TagCollection
    {
        return new TagCollection(Tag::search($request->search ?? null)->paginate(10));
    }
}
