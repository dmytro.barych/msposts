<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Actions\Post\PostDeleteAction;
use App\Actions\Post\PostStoreAction;
use App\Actions\Post\PostToggleLikeAction;
use App\Actions\Post\PostUpdateAction;
use App\Http\Requests\Post\Index;
use App\Http\Requests\Post\Store;
use App\Http\Requests\Post\Update;
use App\Http\Requests\ToggleLikeRequest;
use App\Http\Resources\PostCollection;
use App\Http\Resources\PostResource;
use App\Http\Resources\TagResource;
use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\JsonResponse;

class PostController extends Controller
{
    public function index(Index $request): PostCollection
    {
        return new PostCollection(Post::search($request->search ?? null)->paginate());
    }

    public function store(Store $request, PostStoreAction $action): PostResource
    {
        return new PostResource($action->handle($request->validated()));
    }

    public function show(Post $post): PostResource
    {
        return new PostResource($post);
    }

    public function update(Post $post, Update $request, PostUpdateAction $action): PostResource
    {
        return new PostResource($action->handle($post, $request->validated()));
    }

    public function destroy(Post $post, PostDeleteAction $action): JsonResponse
    {
        return response()->json(['success' => $action->handle($post)]);
    }

    public function toggleLike(Post $post, PostToggleLikeAction $action, ToggleLikeRequest $request): PostResource
    {
        return new PostResource($action->handle($post, (int)$request->user_id));
    }

    public function getPostsByTag(Tag $tag): PostCollection
    {
        return new PostCollection($tag->posts()->paginate());
    }

    public function getPostsByCategory(Category $category): PostCollection
    {
        return new PostCollection($category->posts()->paginate());
    }
}
