<?php

declare(strict_types=1);

namespace App\Http\Requests\Post;

use App\Models\Post;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class Update extends FormRequest
{
    public function rules(): array
    {
        return [
            'title' => [
                'sometimes',
                'min:3',
                'unique_translation:posts'
            ],
            'type' => [
                'sometimes',
                Rule::in(Post::TYPES)
            ],
            'description' => [
                'sometimes',
                'unique_translation:posts'
            ],
            'category_ids' => [
                'sometimes',
                'array'
            ],
            'category_ids.*' => [
                'sometimes',
                'integer',
                'exists:categories,id'
            ],
            'tags' => [
                'sometimes',
                'array',
            ],
            'tags.*' => [
                'sometimes',
                'min:3',
                'alpha_dash'
            ]
        ];
    }
}
