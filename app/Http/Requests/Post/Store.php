<?php

declare(strict_types=1);

namespace App\Http\Requests\Post;

use App\Models\Post;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class Store extends FormRequest
{
    public function rules(): array
    {
        return [
            'user_id' => [
                'required',
            ],
            'type' => [
                'required',
                Rule::in(Post::TYPES)
            ],
            'title' => [
                'required',
                'array',
            ],
            'description' => [
                'sometimes',
                'array'
            ],
            'title.en' => [
                'required',
                'min:3',
                'unique_translation:posts'
            ],
            'description.en' => [
                'sometimes',
                'unique_translation:posts'
            ],
            'category_ids' => [
                'sometimes',
                'array'
            ],
            'category_ids.*' => [
                'sometimes',
                'integer',
                'exists:categories,id'
            ],
            'tags' => [
                'sometimes',
                'array',
            ],
            'tags.*' => [
                'sometimes',
                'min:3',
                'alpha_dash'
            ]
        ];
    }
}
