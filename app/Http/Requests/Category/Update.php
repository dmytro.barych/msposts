<?php

declare(strict_types=1);

namespace App\Http\Requests\Category;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => [
                'required',
                'array',
            ],
            'name.en' => [
                'required',
                'min:3',
                'unique_translation:categories'
            ]
        ];
    }
}
