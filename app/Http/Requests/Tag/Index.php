<?php

declare(strict_types=1);

namespace App\Http\Requests\Tag;

use Illuminate\Foundation\Http\FormRequest;

class Index extends FormRequest
{
    public function rules(): array
    {
        return [
            'search' => [
                'sometimes',
                'min:3'
            ]
        ];
    }
}
