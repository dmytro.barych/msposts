<?php

declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'title' => $this->title,
            'description' => $this->description,
            'likes_count' => $this->likes()->count(),
            'categories' => new CategoryCollection($this->categories),
            'tags' => $this->tags()->pluck('name'),
            'created_at' => $this->created_at,
        ];
    }
}
