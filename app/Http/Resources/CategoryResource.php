<?php

declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    public function toArray($request): array
    {
        $data = [];
        if ($this->resource->relationLoaded('children') && !$this->children->isEmpty()) {
            $data['children'] = new CategoryCollection($this->children->load('children'));
        }

        return array_merge([
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
        ], $data);
    }
}
