<?php

declare(strict_types=1);

namespace App\Traits;

use App\Models\Post;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

trait CategoryRelationsTrait
{
    public function posts(): BelongsToMany
    {
        return $this->belongsToMany(Post::class, 'post_categories');
    }
}
