<?php

declare(strict_types=1);

namespace App\Traits;

use App\Models\Category;
use App\Models\PostLike;
use App\Models\Tag;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait PostRelationsTrait
{
    public function likes(): HasMany
    {
        return $this->hasMany(PostLike::class);
    }

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class, 'post_categories');
    }

    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class, 'post_tags');
    }
}
